
import com.mycompany.jdbc.dao.UserDao;
import com.mycompany.jdbc.helper.DatabaseHelper;
import com.mycompany.jdbc.model.User;

public class main {
    
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        displayDB(userDao);
        System.out.println("----------");
        User selectUser = userDao.get(4);
        System.out.println(selectUser);
//        
//        User newUser = new User("user2", "password", 2, "F");
//        userDao.save(newUser);
//        displayDB(userDao);

        System.out.println("-----------");
//        User updateUser = selectUser;
//        updateUser.setGender("M");
//        userDao.update(updateUser);
//        displayDB(userDao);
//        
//        userDao.delete(selectUser);
        displayDB(userDao);
        DatabaseHelper.close();        
    }
    
    private static void displayDB(UserDao userDao) {
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }
    }
}
