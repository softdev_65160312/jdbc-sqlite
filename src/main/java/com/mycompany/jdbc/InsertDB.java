/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InsertDB {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:test.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection Success!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        String sql = "INSERT INTO employees(FirstName,LastName) VALUES (?,?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, "John");
            pstmt.setString(2, "Wick");
            pstmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(InsertDB.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

    }
}
